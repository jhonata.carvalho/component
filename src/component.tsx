import { View, Text, StyleSheet } from 'react-native'
import React from 'react'

type Props = {
  number1: number
  number2: number
}

export default function Component({ number1, number2 }: Props) {
  const calc = number1 + number2

  return (
    <View style={styles.container}>
      <Text style={{color:"#FFF"}}>{calc}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
    container: {

        justifyContent: 'center',
        alignItems:'center',
        backgroundColor: '#000',
        width:200,
        height:200,
    },
})